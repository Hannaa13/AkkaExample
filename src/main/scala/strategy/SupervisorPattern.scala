package strategy

import java.io.File

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{Actor, ActorLogging, ActorSystem, OneForOneStrategy, Props}
import akka.pattern.{BackoffOpts, BackoffSupervisor}

import scala.io.Source

object SupervisorPattern extends App {

  // проблема - повторение рестарта акторов
  /*
  иммитация работы актора с бд (чтение.запись)
   */
  case object ReadFile

  class FileBasedPersistentActor extends Actor with ActorLogging {
    var daraSource: Source = null

    override def preRestart(reason: Throwable, message: Option[Any]): Unit =
      log.info("Persistent actor restarting")

    override def postStop(): Unit =
      log.warning("Persistent actor has stopped")

    override def preStart(): Unit =
      log.info("Persistent actor starting")

    override def receive: Receive = {
      case ReadFile =>
        if (daraSource == null)
          daraSource = Source.fromFile(new File("src/main/resources/testfiles/important_.txt"))
        log.info("IMPORTANT data" + daraSource.getLines().toList)

    }
  }

  val system = ActorSystem("system")
  // просто работа, без BackoffSupervisor
  // val actor = system.actorOf(Props[FileBasedPersistentActor], "actor")
  //  actor ! ReadFile


  import scala.concurrent.duration._
  // backoff supervisor
  /*
  вызывает FileBasedPersistentActor
  первая попытка рестартануть актора при неудаче через 3 сек
  дальше 2х (6, 12, 24) до 30 сек
  0.2 рандомный фактор
   */

  val simpleSupervisorProps = BackoffSupervisor.props(
    BackoffOpts.onFailure(
      Props[FileBasedPersistentActor],
      "backoffActor",
      3.seconds, // min and max delay
      30.seconds,
      0.2
    )
  )

  //  val simpleBackoff = system.actorOf(simpleSupervisorProps, "backoff")

  val stopProps = BackoffSupervisor.props(
    BackoffOpts.onStop(
      Props[FileBasedPersistentActor],
      "stop",
      3.seconds,
      30.seconds,
      0.2
    ).withSupervisorStrategy(
      OneForOneStrategy() {
        case _ => Stop
      }
    )
  )

  //  val stopSupervisor = system.actorOf(stopProps, "stopSupervisor")
  //  stopSupervisor ! ReadFile
  class EagerActor extends FileBasedPersistentActor {
    override def preStart(): Unit = {
      log.info("eager starting")
      daraSource = Source.fromFile(new File("src/main/resources/testfiles/important_.txt"))
    }
  }

  val repeatedProps = BackoffSupervisor.props(
    BackoffOpts.onStop(
      Props[EagerActor],
      "eager",
      1.second,
      30.seconds,
      0.1
    )
  )
  val repeatedSupervisor = system.actorOf(repeatedProps, "eagerSupervisor")
  /*
repeatedSupervisor  create eagerSupervisor
  - create child eagerActor and eagerActor will die with Exception
  - trigger the supervisor strategy in eagerSupervisor (STOP)
  - backoff will kick in after 1 sec (2,4,8,...30)

   */

}
