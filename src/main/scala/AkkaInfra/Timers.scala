package AkkaInfra

import akka.actor.{Actor, ActorLogging, ActorSystem, Props, Timers}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object Timers extends App {

  case object TimerKey
  case object Start
  case object Messages
  case object Stop
  class MyTimerActor extends Actor with ActorLogging with Timers {
    timers.startSingleTimer(TimerKey, Start, 1.second)

    override def receive: Receive = {
      case Start => log.info("single timer")
        timers.startTimerAtFixedRate(TimerKey, Messages, 1.seconds)
      case Messages => log.info("message")
      case Stop => log.info("stopped")
        timers.cancel()
        context.stop(self)
    }
  }
  val system = ActorSystem("system")
  val myActor = system.actorOf(Props[MyTimerActor])

 system.scheduler.scheduleOnce(8.second) {
   myActor ! Stop
 }

}
