package AkkaInfra

import com.typesafe.config.{Config, ConfigFactory}
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.dispatch.{ControlMessage, PriorityGenerator, UnboundedPriorityMailbox}

object Mailboxes extends App {

  val system = ActorSystem("mailbox", ConfigFactory.load().getConfig("mailboxesDemo"))

  class Simple extends Actor with ActorLogging {
    override def receive: Receive = {
      case msg => log.info(msg.toString)
    }
  }

  /**
   * CUSTOM PRIORITY MAILBOX
   * p0 - most important
   */

  // 1 - mailbox definition
  class SupportTicketPriorityMailbox(settings: ActorSystem.Settings, config: Config) //reflection
    extends UnboundedPriorityMailbox(
      PriorityGenerator {
        case msg: String if msg.startsWith("[P0]") => 0
        case msg: String if msg.startsWith("[P1]") => 1
        case msg: String if msg.startsWith("[P2]") => 2
        case msg: String if msg.startsWith("[P3]") => 3
        case _ => 4
      })

  // 2 - config
  // 3 - dispatcher
  val supportTickerLogger = system.actorOf(Props[Simple].withDispatcher("support-ticker-dispatcher"))

  supportTickerLogger ! "[P3] "
  supportTickerLogger ! "[P0] "
  supportTickerLogger ! "[P1] "

  /**
   * Control-aware mailbox
   */
  // 1 - mark msg as control
  case object ManageTicker extends ControlMessage
  // 2 - configure
  val controlAwareActor = system.actorOf(Props[Simple].withMailbox("control-mailbox"))
  controlAwareActor ! "hi"
  controlAwareActor ! "[P0]"
  controlAwareActor ! ManageTicker
}
