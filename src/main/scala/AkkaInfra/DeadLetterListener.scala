package AkkaInfra

import akka.actor.{Actor, ActorSystem, DeadLetter, Props}

object DeadLetterListener extends  App {

  class DeadLetterListener extends Actor {
    def receive = {
      case d: DeadLetter => println(d)
    }
  }
  val system = ActorSystem("system")
  val listener = system.actorOf(Props[DeadLetterListener])
  system.eventStream.subscribe(listener, classOf[DeadLetter])

}