package AkkaInfra

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.routing.RoundRobinPool

object Routers extends App {
  class Simple  extends Actor with ActorLogging {
    override def receive: Receive = {

      case msg => log.info(msg.toString + context.self.path)
    }
  }
  val system = ActorSystem("routers")
  val routersPool = system.actorOf(RoundRobinPool(3).props(Props[Simple]), "pool")
  for (i <- 1 to 10) routersPool ! s"message $i"

}
